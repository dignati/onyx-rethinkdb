## 0.9.9.0 20-07-2016

* Update to `org.onyxplatform/onyx 0.9.9`

## 0.9.7.0 05-07-2016

* Update to `org.onyxplatform/onyx 0.9.7`

## 0.9.6.3 04-07-2016

* Prevent loosing segments in internal read and retry buffers when done